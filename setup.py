from setuptools import setup


setup(name='test-lib',
      version='0.1',
      description='A test',
      url='http://github.com/storborg/funniest',
      author='Flying Circus',
      author_email='flyingcircus@example.com',
      license='MIT',
      # packages=['test-lib'],
      zip_safe=False)